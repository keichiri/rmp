use std::io;
use std::sync::mpsc::Sender;

use super::command::*;


pub struct CLI {
    sender: Sender<Command>,
}


#[derive(Debug)]
enum RMPError {
    InvalidCommand,
    BadCommandFormat,
    CommandParsing(String),
}





impl CLI {
    pub(super) fn new(sender: Sender<Command>) -> CLI {
        CLI {
            sender: sender,
        }
    }

    pub(super) fn run(&self) {
        let stdin = io::stdin();

        loop {
            let mut command = String::new();
            match stdin.read_line(&mut command) {
                Err(err) => {
                    eprintln!("Error while reading input: {:?}. Terminating", err);
                    break;
                }
                Ok(_) => {
                    let command = command.trim().to_string();
                    if command.len() == 0 {
                        continue;
                    }

                    match self.parse_command(&command) {
                        Ok(command) => self.sender.send(command).unwrap(),
                        Err(err) => eprintln!("Failed to parse command: {}", command),
                    };
                }
            }
        }
    }

    fn parse_command(&self, command_str: &str) -> Result<Command, RMPError> {
        let command_items: Vec<&str> = command_str.split(" ").collect();
        match command_items[0] {
            "play" => self.parse_play(&command_items),
            _ => Err(RMPError::InvalidCommand)
        }
    }

    fn parse_play(&self, command_words: &[&str]) -> Result<Command, RMPError> {
        if command_words.len() != 2 {
            return Err(RMPError::BadCommandFormat);
        }

        let directory = command_words[1].to_string();
        Ok(Command::Play(PlayCommand::Directory(directory)))
    }
}