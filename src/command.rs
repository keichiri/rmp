#[derive(Debug)]
pub(super) enum Command {
    Play(PlayCommand),
}


#[derive(Debug)]
pub(super) enum PlayCommand {
    Directory(String),
}